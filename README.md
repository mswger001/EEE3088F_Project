# EEE3088F_Project
this is the repository containing our stuff for the EEE3088F project



## Building a HAT for a stm32 microprossesor
We have decided to add two sensors that are an analog thermometer and a digital humidity sensor.

## porduction
the bom and pos files are located in the assembly. 
the gerber files are in a zip as well as in the gerber

### Gerald incharge of the Microcontroller interfacing. 
### Tinotenda in charge of Power management. 
### Mutikedzi in charge of Sensing. 



# Introduction:

## In this guide the user will need to write a C language code on STM cube ide that will be inserted on the STM32FO microcontroller. This is done in 4 steps as follows:
•	Install STM cube ide and set it up so that it is ready for coding using C language.

•	Write a C code as indicated on the code reference document and make sure that it works with no errors.

•	Push the code into the STM32FO microcontroller.

•	Insert the microcontroller in the correct pins on the board.

Once you complete these 3 steps, the board will be ready to operate.

## What you will need:

•	STM32FO microcontroller.

•	A PC with a minimum of 4GB ram to ease your work when coding. 

•	A working STM cube ide on your PC.

•	A USB MINIi B cable to insert code into the STM32FO microcontroller.

•	A USB MICRO A cable to power the board.

•	A 4.2V voltage source. 

•	Same PC to display the output.

## How to connect the hardware up:

#### •	Connect the voltage source on the battery holder of the board. 
#### •	Connect the microcontroller on the correct pins on the board.
#### •	Connect the microcontroller to a PC.

# How to run the equivalent of a ‘hello-world’ program that uses the features of PCB:

##### Modify the read and write elements of the code to the storage. Write ‘hello-world’ to memory, read from memory and debug println to output ‘hello-world on on the communication port of the computer.

