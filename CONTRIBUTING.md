•	Other people may contribute back to the project. However, one has to submit a pull request in order to do so.
•	Any issues found with the project can be raised on gitlab.
•	EasyEDA can be used to modify the schematics as an alternative to KiCAD, as it is easier to find footprints for highly specified components. It is also easier to create a Bill Of Materials on EasyEDA.
•	Use of STM Cube IDE is advised when writing the code.

